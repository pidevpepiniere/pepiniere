/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pepiniere.service;

import com.pepiniere.entite.Pepiniere;
import java.sql.ResultSet;
import java.util.List;


/**
 *
 * @author Sami
 */
public interface Iservice <T>{
    
    void insert(T t);
    void delete(T t);
    void update (T t, int id);
    public ResultSet search(T t) ;
    List <T>getAll();
    T getByid(T t);
    
}
