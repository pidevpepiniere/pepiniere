/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pepiniere.service;

import com.pepiniere.entite.Pepiniere;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pepinieree.ConnexionBase;

/**
 *
 * @author Sami
 */
public class PepiniereService implements Iservice<Pepiniere> {
    
    private Connection connection;
    private PreparedStatement pst;
    private Statement pst2;
    private ResultSet rs;

    public PepiniereService (){
        connection=ConnexionBase.getInstance().getCnx();
    }
    
    @Override
    public void insert(Pepiniere t) {
        String requete ="insert into pepinieres(nom_pep,adresse_pep,surface,max,id_user) values (?,?,?,?,?)";
        try {
            pst = connection.prepareStatement(requete);
            
            pst.setString(1, t.getNom_pep());
            pst.setString(2, t.getAdresse_pep());
            pst.setString(3, t.getSurface());
            pst.setInt(4, t.getMax());
            pst.setInt(5, t.getId_user());
            pst.executeUpdate();
            System.out.println("Pépiniére Ajouter avec succé: ");
            
        } catch (SQLException ex) {
            Logger.getLogger(PepiniereService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void delete(Pepiniere t) {
        String requete ="delete from pepinieres where id_pep= "+t.getId_pep();
        
        try {
            pst = connection.prepareStatement(requete);          
            pst.executeUpdate();
            System.out.println("Pépiniére Supprimer avec succé: ");
        } catch (SQLException ex) {
            Logger.getLogger(PepiniereService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void update(Pepiniere t , int id) {
        String requete = "update pepinieres set nom_pep=?,adresse_pep=?,surface=?,max=?,id_user=?  where id_pep= ?";
        try {
            pst= connection.prepareStatement(requete);
            
            pst.setString(1, t.getNom_pep());
            pst.setString(2, t.getAdresse_pep());
            pst.setString(3, t.getSurface());
            pst.setInt(4, t.getMax());
            pst.setInt(5, t.getId_user());
            pst.setInt(6,id);
                    
            pst.executeUpdate();
System.out.println("Pépiniére Modifier avec succé: ");            
        } catch (SQLException ex) {
            Logger.getLogger(PepiniereService.class.getName()).log(Level.SEVERE, null, ex);
        }
        

    }

    @Override
    public List<Pepiniere> getAll() {
        List <Pepiniere> list = new ArrayList <>();
        String requete="select * from pepinieres";
       
        try {
            pst2= connection.createStatement();
            rs=pst2.executeQuery(requete);
            while (rs.next()){
                list.add(new Pepiniere(rs.getInt(1), rs.getString(2),rs.getString(3) , rs.getString(4), rs.getInt(5), rs.getInt(6)));
               
            }
                
                
                
        } catch (SQLException ex) {
            Logger.getLogger(PepiniereService.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    return list;
        
    }

    @Override
    public Pepiniere getByid(Pepiniere t) {
        List <Pepiniere> l = new ArrayList <>();
        String requete="select * from pepinieres where id_pep ="+t.getId_pep();
        try {
            pst2= connection.createStatement();
            rs=pst2.executeQuery(requete);
            while (rs.next()){
                l.add(new Pepiniere(rs.getInt(1), rs.getString(2),rs.getString(3) , rs.getString(4), rs.getInt(5), rs.getInt(6)));
               
            }
            
            
        } catch (SQLException ex) {
            Logger.getLogger(PepiniereService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return t;   //A VERIFIER
    }
        

    @Override
    public ResultSet search(Pepiniere t) {
         rs= null;
        String requete= "SELECT * FROM Product WHERE id_pep LIKE '%" + t.getId_pep() + "%' OR nom_pep LIKE '%" + t.getNom_pep() + 
                "%' or adresse_pep LIKE '%" + t.getAdresse_pep() + 
                "%'  or surface LIKE '%" + t.getSurface() + "%'  or max LIKE '%" + t.getMax() +
                "%'  or id_user LIKE '%" + t.getId_user() + "%' ORDER BY id_pep DESC" ;   ;
        try {
            pst= connection.prepareStatement(requete);
            rs = pst.executeQuery();
            
        } catch (SQLException ex) {
            Logger.getLogger(PepiniereService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return rs ;
    }
    
    
}
