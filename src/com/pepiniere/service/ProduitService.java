/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pepiniere.service;

import com.pepiniere.entite.Produit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pepinieree.ConnexionBase;

/**
 *
 * @author Sami
 */
public class ProduitService implements Iservice<Produit> {
    
    private Connection connection ;
    private PreparedStatement pst;
    private Statement ste ;
    private ResultSet rs;
    
    public ProduitService (){
        connection = ConnexionBase.getInstance().getCnx();
    }
    

    @Override
    public void insert(Produit t) {
        String requete = "insert into produit (type, nom,image_prod,prix,id_pep,disponibilité,description_prod)values (?,?,?,?,?,?,?)";
        try {
            pst = connection.prepareStatement(requete);
            
            pst.setString(1, t.getType());
            pst.setString(2, t.getNom());
            pst.setString(3, t.getImage_prod());
            pst.setFloat(4, t.getPrix());
            pst.setInt(5, t.getId_pep());
            pst.setString(6,t.getDisponibilite());
            pst.setString(7, t.getDescription_prod());
            pst.executeUpdate();
            System.out.println("Produit Ajouter avec succé: ");
            
        } catch (SQLException ex) {
            Logger.getLogger(ProduitService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete(Produit t) {
        String requete ="delete from produit where id_prod= "+t.getId_prod();
        try {
            pst = connection.prepareStatement(requete);
            pst.executeUpdate();
            System.out.println("Produit Supprimer avec succé: ");
        } catch (SQLException ex) {
            Logger.getLogger(ProduitService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void update(Produit t , int id) {
        String requete = "update produit set type=?, nom=?,image_prod=?,prix=?,id_pep=?,disponibilité=?,description_prod=? where id_prod=?";
        try {
            pst = connection.prepareStatement(requete);
            
            pst.setString(1, t.getType());
            pst.setString(2, t.getNom());
            pst.setString(3, t.getImage_prod());
            pst.setFloat(4, t.getPrix());
            pst.setInt(5, t.getId_pep());
            pst.setString(6,t.getDisponibilite());
            pst.setString(7, t.getDescription_prod());
            pst.setInt(8, id);
            pst.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(ProduitService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public List<Produit> getAll() {
        List <Produit> list = new ArrayList<>();
        String requete = "select * from produit";
        try {
            ste=connection.createStatement();
            rs=ste.executeQuery(requete);
            while (rs.next()){
                list.add(new Produit(rs.getInt("id_prod"), rs.getString(2),rs.getString(3) , rs.getString(4), rs.getFloat(5), rs.getInt(6),rs.getString(7),rs.getString(8)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProduitService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    return list;
    }

    @Override
    public Produit getByid(Produit t) {
 List <Produit> list = new ArrayList <>();
        String requete="select * from pepinieres where id_pep ="+t.getId_prod();
        try {
            ste= connection.createStatement();
            rs=ste.executeQuery(requete);
            while (rs.next()){
                list.add(new Produit(rs.getInt(1), rs.getString(2),rs.getString(3) , rs.getString(4), rs.getFloat(5), rs.getInt(6),rs.getString(7),rs.getString(8)));
               
            }
            
            
            
        } catch (SQLException ex) {
            Logger.getLogger(ProduitService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return t ;             //A VERIFIER
    }

    @Override
    public ResultSet search(Produit t) {
        rs = null;
        String requete ="SELECT * FROM Product WHERE id_prod LIKE '%" + t.getId_prod() + "%' OR type LIKE '%" + t.getType() + 
                "%' or nom LIKE '%" + t.getNom() + 
                "%'  or image_prod LIKE '%" + t.getImage_prod() + "%'  or prix LIKE '%" + t.getPrix() +
                "%'  or id_pep LIKE '%" + t.getId_pep() + "%' or disponibilite LIKE '"+t.getDisponibilite()+
                "%' or description_prod LIKE '%" +t.getDescription_prod()+"%' ORDER BY id_pep DESC" ;   ;
;
        
        try {
            pst=connection.prepareStatement(requete);
            rs= pst.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(ProduitService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }
    

    
}
