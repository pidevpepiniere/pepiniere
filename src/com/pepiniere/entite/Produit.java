/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pepiniere.entite;

import java.util.Objects;

/**
 *
 * @author Sami
 */
public class Produit {
        
      private int  id_prod ;	
      private String type ;	
      private String nom ;	
      private String image_prod	;
      private float prix  ;	
      private int id_pep;	
      private String disponibilite;	
      private String description_prod;

    public Produit() {
    }

    public Produit(int id_prod, String type, String nom, String image_prod, float prix, int id_pep, String disponibilité, String description_prod) {
        this.id_prod = id_prod;
        this.type = type;
        this.nom = nom;
        this.image_prod = image_prod;
        this.prix = prix;
        this.id_pep = id_pep;
        this.disponibilite = disponibilite;
        this.description_prod = description_prod;
    }

    public Produit(String type, String nom, String image_prod, float prix, int id_pep, String disponibilite, String description_prod) {
        this.type = type;
        this.nom = nom;
        this.image_prod = image_prod;
        this.prix = prix;
        this.id_pep = id_pep;
        this.disponibilite = disponibilite;
        this.description_prod = description_prod;
    }
    

    public int getId_prod() {
        return id_prod;
    }

    public void setId_prod(int id_prod) {
        this.id_prod = id_prod;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getImage_prod() {
        return image_prod;
    }

    public void setImage_prod(String image_prod) {
        this.image_prod = image_prod;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public int getId_pep() {
        return id_pep;
    }

    public void setId_pep(int id_pep) {
        this.id_pep = id_pep;
    }

    public String getDisponibilite() {
        return disponibilite;
    }

    public void setDisponibilité(String disponibilite) {
        this.disponibilite = disponibilite;
    }

    public String getDescription_prod() {
        return description_prod;
    }

    public void setDescription_prod(String description_prod) {
        this.description_prod = description_prod;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.id_prod);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Produit other = (Produit) obj;
        if (!Objects.equals(this.id_prod, other.id_prod)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Produit{" + "id_prod=" + id_prod + ", type=" + type + ", nom=" + nom + ", image_prod=" + image_prod + ", prix=" + prix + ", id_pep=" + id_pep + ", disponibilite=" + disponibilite + ", description_prod=" + description_prod + '}';
    }

    

    

    
}
