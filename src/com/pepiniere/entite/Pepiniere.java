/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pepiniere.entite;

/**
 *
 * @author Sami
 */
public class Pepiniere {
    private int id_pep;
    private String nom_pep;
    private String adresse_pep;
    private String surface ;
    private int max;
    private int id_user;

    public Pepiniere() {
    }

   
    
    public Pepiniere(int id_pep, String nom_pep, String adresse_pep, String surface, int max, int id_user) {
       this.id_pep = id_pep;
        this.nom_pep = nom_pep;
        this.adresse_pep = adresse_pep;
        this.surface = surface;
        this.max = max;
        this.id_user = id_user;
    }

    public Pepiniere(String nom_pep, String adresse_pep, String surface, int max, int id_user) {
        this.nom_pep = nom_pep;
        this.adresse_pep = adresse_pep;
        this.surface = surface;
        this.max = max;
        this.id_user = id_user;
    }
    

    public int getId_pep() {
        return id_pep;
    }

    public void setId_pep(int id_pep) {
        this.id_pep = id_pep;
    }

    public String getNom_pep() {
        return nom_pep;
    }

    public void setNom_pep(String nom_pep) {
        this.nom_pep = nom_pep;
    }

    public String getAdresse_pep() {
        return adresse_pep;
    }

    public void setAdresse_pep(String adresse_pep) {
        this.adresse_pep = adresse_pep;
    }

    public String getSurface() {
        return surface;
    }

    public void setSurface(String surface) {
        this.surface = surface;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pepiniere other = (Pepiniere) obj;
        if (this.id_pep != other.id_pep) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Pepiniere{" + "id_pep=" + id_pep + ", nom_pep=" + nom_pep + ", adresse_pep=" + adresse_pep + ", surface=" + surface + ", max=" + max + ", id_user=" + id_user + '}';
    }
    
    
    
    
    
    
    
    
}
