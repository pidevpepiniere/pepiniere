/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pepinieree;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sami
 */
public class ConnexionBase {

    /**
     * @param args the command line arguments
     */
    private  String url ="jdbc:mysql://localhost:3306/pepiniere";
        private  String login="root";
        private  String pwd="";
        private  Connection cnx;
        
        private static ConnexionBase instance ;
        
    private ConnexionBase(){
        try {
            // TODO code application logic here
            cnx=DriverManager.getConnection(url, login, pwd);
            System.out.println ("connexion etablie");
        } catch (SQLException ex) {
            Logger.getLogger(ConnexionBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        
}
    public static ConnexionBase getInstance (){
        if (instance==null)
            instance=new ConnexionBase();
        return instance;
    }

    public Connection getCnx() {
        return cnx;
    }
    
}
